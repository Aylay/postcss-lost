/*----------- REQUIRE ----------*/
// Gulp
var gulp          = require('gulp');
var gutil         = require('gulp-util');
var runSequence   = require('run-sequence');

// Sass/CSS stuff
var postcss       = require('gulp-postcss');
var atImport      = require("postcss-import")
var sass          = require('gulp-sass');
var prefix        = require('autoprefixer');
var cssnano       = require('cssnano');

// Stats and Things
var size          = require('gulp-size');

// Lost
var lost          = require('lost');

// Rucksack
var rucksack      = require('rucksack-css');

// Font Magician
var magician      = require('postcss-font-magician');


/*--------- VARIABLES -------*/
var dev_processors = [
  lost,
  magician({
    protocol: 'http:'
  }),
  atImport(),
  rucksack,
  prefix({
    browsers:['last 10 version']
  }),
];

var prod_processors = [
  lost,
  magician({
    protocol: 'http:'
  }),
  atImport(),
  rucksack,
  prefix({
    browsers:['last 10 version']
  }),
  cssnano(),
];


/*----------- TASKS ----------*/
// compile all your Sass
gulp.task('scss', function (){
  return gulp.src('dev/scss/*.scss')
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(postcss(dev_processors))
    .pipe(gulp.dest('dev/css/'));
});

// Minify
gulp.task('minify', function (){
  return gulp.src('dev/css/*.css') 
    .pipe(postcss(prod_processors))
    .on('error', gutil.log)
    .pipe(gulp.dest('prod/css/'));
});
 
// Size
gulp.task('size', function (){
  return gulp.src('prod/**/*')
    .pipe(size())
    .pipe(gulp.dest('prod/'));

});

// Prod
gulp.task('prod', function(){
  runSequence('minify', 'size');
}); 

// Default
gulp.task('default', function(){
  // watch me getting scss
  gulp.watch("dev/scss/**/*.scss", ['scss']);
});

